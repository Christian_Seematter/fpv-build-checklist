// # categories #
// "quad"
// "assembly"
// "spare"
// "gear"
// "tool"
// "optional"

partsData = {
    "1": {
       "id": "1",
       "name": "Frame",
       "category": "quad"
   }, 
    "2": {
       "id": "2",
       "name": "ECU",
       "category": "quad"
   }, 
    "3": {
       "id": "3",
       "name": "4x Motoren",
       "category": "quad"
   }, 
    "4": {
       "id": "4",
       "name": "FC",
       "category": "quad"
   }, 
    "5": {
       "id": "5",
       "name": "VTX",
       "category": "quad"
   }, 
    "6": {
       "id": "6",
       "name": "VTX Antenna",
       "category": "quad"
   }, 
    "7": {
       "id": "7",
       "name": "FPV Cam",
       "category": "quad"
   }, 
    "8": {
       "id": "8",
       "name": "RX module (RC Receiver)",
       "category": "quad"
   }, 
    "9": {
       "id": "9",
       "name": "(optional) IPEX4 to SMA adapter",
       "category": "quad"
   }, 
    "10": {
       "id": "10",
       "name": "recording Cam",
       "category": "quad"
   }, 
    "11": {
       "id": "11",
       "name": "Props",
       "category": "quad"
   }, 
    "12": {
       "id": "12",
       "name": "Batteries",
       "category": "quad"
   }, 
    "13": {
       "id": "13",
       "name": "Power Cable",
       "category": "assembly"
   }, 
    "14": {
       "id": "14",
       "name": "TX60 plug",
       "category": "assembly"
   }, 
    "15": {
       "id": "15",
       "name": "M3? screws",
       "category": "assembly"
   }, 
    "16": {
       "id": "16",
       "name": "Heatshrink",
       "category": "assembly"
   }, 
    "17": {
       "id": "17",
       "name": "Motoren",
       "category": "spare"
   }, 
    "17": {
       "id": "17",
       "name": "Frame",
       "category": "spare"
   }, 
    "17": {
       "id": "17",
       "name": "ECU",
       "category": "spare"
   }, 
    "17": {
       "id": "17",
       "name": "Props",
       "category": "spare"
   },    
    "18": {
       "id": "18",
       "name": "Goggles",
       "category": "gear"
   },    
   "19": {
        "id": "19",
        "name": "TX",
        "category": "gear"
    },    
   "20": {
        "id": "20",
        "name": "M3 Screwdriver",
        "category": "tool"
    },    
   "21": {
        "id": "21",
        "name": "Glue gun",
        "category": "optional"
    },    
};