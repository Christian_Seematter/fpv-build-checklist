$(document).ready(function () {

  function generatePartListEntry(part) {
    var html = '';
    
    var saved_amount = localStorage.getItem(part.id + '_amount');
    var saved_name = localStorage.getItem(part.id + '_name');

    saved_amount = saved_amount ? saved_amount : '';
    saved_name = saved_name ? saved_name : '';

    html += '\
            <tr>\
                <td class="td1">'+ part.name +'</td>\
                <td class="td2"><input type="number" name="'+ part.id + '_amount" full_name="' + part.name + '" class="amount" value="' + saved_amount + '"></td>\
                <td class="td3"><input type="input" name="'+ part.id + '_name" full_name="' + part.name + '" class="name" value="' + saved_name + '">';
    html += '</td>';

    return html;
  }

  sortJsonArrayByProperty(partsData, 'attributes.id', 1);

  // Write the parts onto the website per category
  $.each(partsData, function (key, part) {
    if (part.category == 'quad') {
      $("#quad").append(generatePartListEntry(part));
    }
    if (part.category == 'assembly') {
      $("#assembly").append(generatePartListEntry(part));
    }
    if (part.category == 'spare') {
      $("#spare").append(generatePartListEntry(part));
    }
    if (part.category == 'gear') {
      $("#gear").append(generatePartListEntry(part));
    }
    if (part.category == 'tool') {
      $("#tool").append(generatePartListEntry(part));
    }
    if (part.category == 'optional') {
      $("#optional").append(generatePartListEntry(part));
    }
  });
  
  // ave changes in localStorage
  $("input").bind("input", function( event ) {
    savePartAmount(event.target);
  });
  $("#radio.pickup").change(function( event ) {
    savePartAmount(event.target);
  });
  $("#radio.send").change(function( event ) {
    savePartAmount(event.target);
  });
  $("textarea").bind("input", function( event ) {
    savePartAmount(event.target);
  });

});