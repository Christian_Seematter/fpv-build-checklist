function localStoreWorking() {
  try {
    localStorage.setItem("localStoreTest", "works");
  }
  catch (e) {
    return false;
  }
  return true;
}

function sortJsonArrayByProperty(objArray, prop, direction) {
  if (arguments.length < 2) throw new Error("sortJsonArrayByProp requires 2 arguments");
  var direct = arguments.length > 2 ? arguments[2] : 1; //Default to ascending

  if (objArray && objArray.constructor === Array) {
    var propPath = (prop.constructor === Array) ? prop : prop.split(".");
    objArray.sort(function (a, b) {
      for (var p in propPath) {
        if (a[propPath[p]] && b[propPath[p]]) {
          a = a[propPath[p]];
          b = b[propPath[p]];
        }
      }
      // convert numeric strings to integers
      a = a.match(/^\d+$/) ? +a : a;
      b = b.match(/^\d+$/) ? +b : b;
      return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
    });
  }
}

function savePartAmount(target){
  if(target.value){
    try{
      localStorage.setItem(target.name, target.value);
    }
    catch(error){
      // Keep calm and carry on!
      // This try is needed for private browsing and iOS.
    }
  }
  else{
    localStorage.removeItem(target.name);
  }
}

